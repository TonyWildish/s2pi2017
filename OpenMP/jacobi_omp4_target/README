== Description ==

Provides different version of jacobi,
    -OpenMP 3.1 version of Jacobi (jacobi.f)
    -OpenMP 4.5 version of Jacobi (jacobi-accel.f)

== How to do the exercises ==

== Compilation and running on the training machines with training accounts ==
Go to the jacobi folder,
    cd jacobi
    
=== Compilation ===

The run the build script,
    source build_job.sh <compiler>
compiler = intel | cray
The build script supports Blue Waters (Cray XK), Titan (Cray XK7), Chester (Cray XK7), Eos (Cray XC31), and Beacon (Intel Xeon Phi).

On Blue Waters only cray compiler is supported.

When the build script is successful, it will build three versions of the Jacobi code:
    - j.<compiler>: OpenMP 3.1 version of Jacobi
    - jacc.<compiler>: OpenMP 4.5 version of Jacobi, compiled to run on the accelerator
    - jacc.host.<compiler>: OpenMP 4.5 version of Jacobi compiled to run on the host (CPU)    

=== Running ===

Batch mode(easy to do, same command will work for all machines)
---------------------------------------------------------------
    source run_job.sh <executable name>

    e.g. source run_job.sh jacc.cray
    
Interactive mode (a bit more work, but more useful for development and testing)
-------------------------------------------------------------------
    For Blue Waters:
    ----------------
    1. Allocate a compute node for 1 hour,
        qsub -I -l nodes=2:ppn=16:xk -l walltime=01:00:00
    2. Once allocated, run the program like any executable
        ./jacc.cray
        
