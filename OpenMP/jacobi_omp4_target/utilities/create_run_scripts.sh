#!/bin/bash -l

function print_usage() {
    echo ""
    echo "Usage:"
    echo "      ./create_run_scripts.sh <matsize>"
    echo "          matsize:    MxN, with M and N as integers"
    echo ""
    echo "      Options:"
    echo "          -h | --help: Prints usage."
    echo ""
}

if [ $# -lt 1 ]
then
    echo "A matrix size is required."
    print_usage
    exit
fi

if [[ $1 == '-h' || $1 == '--help' ]]
then
    print_usage
    exit
fi

MATSIZE=$1
OUTFILE=run_jacobi_$MATSIZE.sh

echo "Creating run script for matrix size $1..."

echo "#!/bin/bash -l" > $OUTFILE
echo "#PBS -l nodes=1" >> $OUTFILE
echo "#PBS -l walltime=00:45:00" >> $OUTFILE
echo "#PBS -N omp4j.$MATSIZE" >> $OUTFILE
echo "#PBS -j oe" >> $OUTFILE
echo "#PBS -A UT-TENN0083" >> $OUTFILE
echo "" >> $OUTFILE
echo "cd \$PBS_O_WORKDIR" >> $OUTFILE
echo "" >> $OUTFILE
echo "module list" >> $OUTFILE
echo "" >> $OUTFILE
echo "INFILE=../inputs/input.$MATSIZE" >> $OUTFILE
echo "" >> $OUTFILE
echo "#export OMP_NUM_THREADS=16" >> $OUTFILE
echo "#export MIC_ENV_PREFIX=MIC" >> $OUTFILE
echo "#export MIC_OMP_NUM_THREADS=236" >> $OUTFILE
echo "#export OFFLOAD_REPORT=1" >> $OUTFILE
echo "" >> $OUTFILE
echo "echo \"\"" >> $OUTFILE
echo "echo \"Jacobi OMP3 on CPU\"" >> $OUTFILE
echo "time -p ./j.intel < \$INFILE" >> $OUTFILE
echo "" >> $OUTFILE
echo "echo \"\"" >> $OUTFILE
echo "echo \"Jacobi OMP4 on CPU\"" >> $OUTFILE
echo "time -p ./jacc.host.intel < \$INFILE" >> $OUTFILE
echo "" >> $OUTFILE
echo "echo \"\"" >> $OUTFILE
echo "echo \"Jacobi OMP4 on Xeon Phi\"" >> $OUTFILE
echo "time -p ./j.openacc < \$INFILE" >> $OUTFILE
