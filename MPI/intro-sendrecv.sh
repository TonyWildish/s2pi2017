#!/bin/sh
#SBATCH -N 1
#SBATCH --qos=jgi
#SBATCH --time 00:01:00
#SBATCH --constraint haswell
#SBATCH --output slurm.intro-sendrecv.out
#SBATCH --error  slurm.intro-sendrecv.err

mpicc=mpicc
mpiexec=mpiexec
if [ "$NERSC_HOST" == "cori" ]; then
  module load cray-mpich # openmpi
  mpicc=cc
  mpiexec=srun
else
  export TMPDIR=/tmp # needed for unix socket
fi

$mpicc intro-sendrecv.c -o intro-sendrecv
$mpiexec -n 16 ./intro-sendrecv
