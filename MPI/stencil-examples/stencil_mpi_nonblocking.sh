#!/bin/sh
#SBATCH -N 1
#SBATCH --qos=jgi
#SBATCH --time 00:01:00
#SBATCH --constraint haswell
#SBATCH --output slurm.mpi_nonblocking.out
#SBATCH --error  slurm.mpi_nonblocking.err

mpicc=mpicc
mpiexec=mpiexec
if [ "$NERSC_HOST" == "cori" ]; then
  module load cray-mpich # openmpi
  mpicc=cc
  mpiexec=srun
else
  export TMPDIR=/tmp # needed for unix socket
fi

$mpicc -g printarr_par.c -c -lm -o printarr_par.o
$mpicc stencil_mpi_nonblocking.c printarr_par.o -lm -o stencil_mpi_nonblocking
$mpiexec -n 16 ./stencil_mpi_nonblocking
