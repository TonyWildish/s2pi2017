#!/bin/bash -l
## this exercise will potentially take some time - we'll be polite by
## using as few nodes as we can for as short time as we can in each job
## Select one at a time from the -N -t combinations:
#SBATCH -N 1 -t 10
##SBATCH -N 2 -t 5
##SBATCH -N 4 -t 5
#SBATCH -L SCRATCH
#SBATCH -C haswell
#SBATCH -J ex5
##SBATCH --reservation=STPI_hsw

# In this exercise we will run miniFE at a few different scales for each of a 
# few different sizes and plot the performance in terms of MFLOPS (Handily,
# miniFE reports a FLOP count for each run, based on the problem size)

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $SCRATCH/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
training_dir=Work/Tutorials/Petascale-Institute/Exercises/s2pi2017/ScalingProfiling

# path to the miniFE executable we built at step 1:
ex=$HOME/$training_dir/ex1-scaling/build/miniFE.x

# A Cori Haswell node has 32 cores, each with 2 hyperthreads. Our Slurm 
# setup considers each hyperthread to be a CPU, but for this exercise we want
# each MPI task to have its own core. The following recipe calculates how
# many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
# a "cpu" in Slurm is a hyperthread:
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((SLURM_CPUS_ON_NODE/cpus_per_task))
max_mpi_ranks=$((SLURM_NNODES * max_mpi_per_node))

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

declare -a sizes=(100 200 300 400)
for j in $(seq 1 ${#sizes[@]} ); do
  sz=${sizes[$((j-1))]}
  # we'll make a separate gnuplot data file for each problem size:
  # this time we won't use $SLURM_JOB_ID in the name, so multiple
  # jobs can append the same file:
  timing_file=$SLURM_SUBMIT_DIR/sz${sz}-timings.dat
  if [[ ! -e $timing_file ]] ; then
    # write a heading line, if there isn't already one:
    echo "#nranks" $'\t' "flopcnt" $'\t' "walltime" $'\t' "bm_time" $'\t' "CG_MFlops" >> $timing_file
  fi

  cmd="$ex -nx $sz -ny $sz -nz $sz"

  # we'll start the bigger datasets at progressively higher core counts:
  for i in $(seq $j 10) ; do
    nranks=$((2**i)) # 1, 2, 4, 8, ...
    [[ $nranks -gt $max_mpi_ranks ]] && break # max we can scale to in this job
    nnodes=$(( (nranks+max_mpi_per_node-1)/max_mpi_per_node))
    # rather than holding 2 nodes while only using 1, we'll submit this job 
    # separately for each node count, and skip the runs that are not matched 
    # to the current node count:
    [[ $nnodes -lt $SLURM_NNODES ]] && continue

    label=sz${sz}-${SLURM_NNODES}n-${nranks}mpi-$SLURM_JOB_ID

    # run in a unique directory under $SCRATCH:
    rundir=$SCRATCH/$training_dir/ex5-weakscaling/$label
    mkdir -p $rundir
    cd $rundir

    echo "starting miniFE sz=${sz} on with $nranks MPI processes at `date`"
    tm=$( { time srun -N$nnodes -n$nranks -c$cpus_per_task --cpu_bind=cores $cmd > stdout 2> stderr ; } 2>&1 )

    # find the flopcount and report CG Mflops in the yaml output:
    flopcnt=$(awk -F: '/Total CG Flops/ { print $2 }' *.yaml)
    cg_mflops=$(awk -F: '/Total CG Mflops/ { print $2 }' *.yaml)
    bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
    echo "  $nranks" $'\t' $flopcnt $'\t' $tm $'\t' $bm_tm $'\t' $cg_mflops >> $timing_file
  done
done
echo "finished miniFE runs at `date`"

