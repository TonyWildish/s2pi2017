Scaling to Petascale Institute - Scaling and Profiling Workshop
===============================================================

Exercise 2: Profiling miniFE with TAU
-------------------------------------

In this exercise we will rebuild our miniFE executable, this time instrumented 
with TAU. We'll run it at a few scales and use paraprof (and pprof) to look at
how different parts of the application scale.

.. _Step 1:

Step 1: Prepare the build environment for TAU
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First we'll need the TAU scripts and libraries in our PATHs. On Cori, 
``module load tau`` will prepare this (try ``module show tau`` to see what 
loading the module does)

Also, ``module unload darshan``

``cd`` into the ``build`` directory and look at the ``Makefile`` there. In the
added section we've overridden the compiler names with TAU wrapper scripts - 
this is common practice for tools which instrument code. TAU comes with a 
set of Makefiles corresponding to different instrumentation options - in this
example we're using the Intel suite as the underlying compilers, instrumenting
with PDT (the instrumenter included with TAU) and building for MPI, so we have
selected an appropriate TAU Makefile (remember, the TAU_MAKEFILE tells TAU how 
to instrument, it is distinct from the application Makefile)

If you run ``make`` without having set the TAU_MAKEFILE and TAU_OPTIONS 
environment variables, "make" will print some errors showing what to set

After setting these, make the new executable::

  $ module load tau
  $ cd build
  $ # on Blue Waters, these will be slightly different, run make without
  > # any options to get the commands:
  $ export TAU_MAKEFILE=$TAULIBDIR/Makefile.tau-intel-mpi-pdt
  $ export TAU_OPTIONS="-optKeepFiles -optVerbose -optPDTInst -optHeaderInst"
  $ make 

If everything worked, you should see an executable ``miniFE-tau.x`` in your current
(``build``) directory.


.. _Step 2:

Step 2: Run the instrumented miniFE 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``cd ..`` back to this directory and take a look in ``cori-jobscript.sh`` or
``bw-jobscript``. (We again have a ``cori-jobscript-advanced.sh``, with 
options for additional and alternative experiments)

The script again runs a sequence of job steps, this time for 1, 2, 16 and 32
MPI ranks. Each run should result in a set of profile files (one per rank).  

The slurm job output will include the wallclock time for each run. Compare it
to the walltime you plotted for corresponding runs without TAU - you'll notice
that profiling adds some overhead to the run time - this is something to keep 
in mind when profiling any application. 

Submit the job with::

  $ sbatch cori-jobscript.sh
  Submitted batch job 5381580

Or::

  $ qsub bw-jobscript.sh

.. _Step 3:

Step 3: Explore profile at different scales
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TAU includes ``paraprof``, an X application for exploring TAU-generated 
performance profiles. If you are logged into the cluster over a high-latency
network connection, X performance can be painfully poor - on Cori you can 
mitigate a slow network by loggin in via NX_.

.. _NX: http://www.nersc.gov/users/connecting-to-nersc/using-nx/

``cd`` to a folder with a TAU profile (probably something like: 
``$SCRATCH/path/to/ex2//tau_profile-*``)) and run ``paraprof`` to explore the
profile::

  $ module load tau
  $ cd $SCRATCH/path/to/ex2/tau_profile-sz200-1n-2mpi-5399296/
  $ paraprof &

We'll go over some parts of the GUI in the presentation, but in the meantime
use the GUI to explore the profile (note that a right-click frequently opens a
useful menu).

An optional extra exercise is to open multiple profiles together and compare 
the profiles for different processor counts. See ``cori-jobscript-advanced.sh``
if you would like to try this.

.. _Text-based profile viewer:

TAU includes ``pprof``, which writes a text report to stdout. By default this 
report has sections for each task in the profile, so it will be very long - you
will want to pipe it to a file or pager::

  $ module load tau
  $ cd $SCRATCH/path/to/ex2/tau_profile-sz200-1n-2mpi-5399296/
  $ pprof --help
  pprof: invalid option -- '-'
  pprof: invalid option -- 'h'
  usage: pprof [-c|-b|-m|-t|-e|-i|-v] [-r] [-s] [-n num] [-f filename] [-p] [-l] [-d] [node numbers]
  ...

  $ pprof | less -FX
  ...

You might want to print just a summary (``-s``) or the top 5 routines (``-n 5``)
sorted by exclusive-time (``-m``)::

  $ pprof -s -n 5 -m 



.. _Step 4:

Step 4: Identify some parts of the code that scale well, and poorly
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Visually comparing profiles can give some hints about which routines scale well
and which do not. Another option is to plot the scaling of key routines. TAU 
doesn't have a built-in function for this, but with a little python scripting 
we can generate a table that gnuplot can read and plot.

A python script to do this is ``parse_pprofs.py`` in this directory. If you 
call it with ``-t 5`` it will look only for the first 5 routines in each 
process, then it looks for a sequence of ``-n $nprocs -d $dir_with_profile``
argument pairs. Here's an example of calling it::

  $ module load tau
  $ run1p=/path/to/1p/run  # eg $SCRATCH/path/to/ex2/tau_profile-sz200-1n-1mpi-5399296/
  $ run2p=/path/to/2p/run  
  $ run16p=/path/to/16p/run  
  $ run32p=/path/to/32p/run  
  $ python ./parse_pprofs.py -t 6 -n 1 -d $run1p -n 2 -d $run2p -n 16 -d $run16p -n 32 -d $run32p > routine_scaling.dat
  $ cat routine_scaling.dat
  nproc others matvec_std perform_element_loop impose_dirichlet init_matrix generate_matrix_structure waxpby MPI_Allreduce
  1     4755   60734      28580                18062            9483        2890                      6815   0
  2     4189   30358      14217                7167             4747        1454                      3201   0
  16    2117   6310       2251                 704              1440        0                         814    789
  32    2044   6070       992                  306              697         0                         803    392

We can now use gnuplot to plot the scaling of the first few routines::

  $ gnuplot
  gnuplot> f="routine_scaling.dat"
  gnuplot> nroutines=6
  gnuplot> set key autotitle columnheader
  gnuplot> set key outside top right
  gnuplot> set xlabel "nproc"
  gnuplot> set ylabel "msec"
  gnuplot> set logscale
  gnuplot> plot for [i=2:nroutines+1] f u 1:i w lp


You can pause here and we'll go over some typical results in the slides.

.. _Extensions:

Extension 1: Callpath Profiling
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In ``cori-jobscript-advanced.sh`` is an option for callpath profiling. This 
enables a window in TAU to show a calpath graph of the run, in which time spent
in each routiine is indicated by color and time spent in each routine and all 
of its callees is indicated by the size of the box. Try it out.

Extension 2: Dynamic linking 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TAU can profile a dynamically-linked executable without needing to instrument the 
source code. Try unloading the TAU module from your environment and building 
miniFE with dynamic linking (see ``build/Makefile``), and run the resulting 
executable with tau_exec (see ``cori-jobscript-advanced.sh``)


