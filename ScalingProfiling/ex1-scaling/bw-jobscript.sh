#!/bin/bash -l
#PBS -l nodes=1:ppn=32:xe
#PBS -l walltime=15:00
#PBS -j oe
#PBS -N ex1

# In this exercise, we will run a small miniFE job at a variety of scales
# and plot the time and speedup against the number of MPI ranks. For now,
# we'll stay within a single node.

# This script assumes that the training tar file is unpacked into 
#   $HOME/$training_dir
# and that the jobs will be run in
#   $HOME/scratch/$training_dir
# If you unpacked the tar file to somewhere else, change this line accordingly:
training_dir=${TUT_PATH:-s2pi2017/ScalingProfiling}
SCRATCH=$HOME/scratch
. /opt/modules/default/init/bash
module swap PrgEnv-cray PrgEnv-intel

cd $PBS_O_WORKDIR

# path to the miniFE executable we built:
ex=$HOME/$training_dir/ex1-scaling/build/miniFE.x

# A 150x150x150 miniFE job should finish within 5 minutes on a single core 
# of a BW node
sz=150
cmd="$ex -nx $sz -ny $sz -nz $sz"

# Each BlueWaters core has 2 hyperthreads, which PBS views as 2 CPUs, but 
# for this exercise we want each MPI task to have its own core. The following
# recipe calculates how many MPI ranks we can run on a given number of nodes:
max_tasks_per_core=1
hyperthreads_per_core=$(lscpu | awk '/^Thread\(s\) per core/ {print $NF}')
cpus_per_node=$(lscpu | awk '/^CPU\(s\):/ {print $NF}')
cpus_per_task=$(( hyperthreads_per_core/max_tasks_per_core ))
max_mpi_per_node=$((cpus_per_node/cpus_per_task))
max_mpi_ranks=$((PBS_NUM_NODES * max_mpi_per_node))

# make the output of 'time' command easy to plot:
TIMEFORMAT=%R

# .. and prepare a gnuplot data file:
timing_file=$PBS_O_WORKDIR/timings-$PBS_JOBID.dat
echo "#nranks" $'\t' "bm_time" $'\t' "walltime" >> $timing_file

# For expediency, we'll do the whole series of scaling runs in the same job
for i in $(seq 0 10) ; do
  nranks=$((2**i)) # 1, 2, 4, 8, ...
  [[ $nranks -gt $max_mpi_ranks ]] && break # max we can scale to in this job

  # make the run output easy to identify:
  label=sz${sz}-${PBS_NUM_NODES}n-${nranks}mpi-$PBS_JOBID
  # run in a unique directory under $SCRATCH:
  rundir=$SCRATCH/$training_dir/ex1-scaling/$label
  mkdir -p $rundir
  cd $rundir

  echo "starting miniFE with $nranks MPI processes on $PBS_NUM_NODES nodes at `date`"
  
  # to run the actual application we need:
  #   aprun -n $nranks $cmd > stdout 2> stderr
  # We're interested in the total wallclock time each run takes, so we'll wrap the 
  # whole command in a call to bash's 'time' function, and capture it's output
  # in a variable:
  tm=$( { time aprun -n $nranks $cmd > stdout 2> stderr ; } 2>&1 )

  # if the run succeeded, we should have a .yaml file in the $rundir with various
  # metrics about how the run went. We'll extract its report of the total run
  # time, and append a gnuplot data file with the timing info:
  bm_tm=$(awk -F: '/Total Program Time/ { print $2 }' *.yaml)
  echo "  $nranks" $'\t' $bm_tm $'\t' $tm >> $timing_file

done
echo "finished miniFE runs at `date`"
