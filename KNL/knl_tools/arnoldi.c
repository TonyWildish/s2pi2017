/*
 * =====================================================================================
 *
 *       Filename:  arnoldi.c
 *
 *    Description:  Kyrlov Subspace computation using arnoldi iteration
 *        Created:  6/23/2017      
 *        Version:  1.0
 *         Author:  Richard Todd Evans
 *   Organization:  TACC
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <math.h>
#include <hbwmalloc.h>

// This example uses Arnoldi iteration
// to construct a Krylov Subspace of a
// Matrix A parallelized with OpenMP

int main()
{
  int i, j, in, out;
  int N = 20000;   // Dimension of square matrix A
  int M = 10;      // Size of Krylov Subspace to build
  
  double *A = (double *) malloc(N*N*sizeof(double)); // Matrix A
  double v[M][N];  // Subspace vectors 
  double h[M];     
  double w[N];

  double start,end;
  double delta;

  double checksum = 0;
  double norm = 0;
  double tmp;

  // Build initial normalized vector
  for (i = 0; i < N; i++)
    v[0][i] = i;
  for (i = 0; i < N; i++)
    norm += v[0][i]*v[0][i];
  norm = sqrt(norm);
  for (i = 0; i < N; i++)
    v[0][i] /= norm;
  for (i = 0; i < N; i++)
    checksum += v[0][i]*v[0][i];
  printf("check sum for normalization: %0.2f\n", checksum);
  
  // Build random matrix A
  srand(1);
  for (i = 0; i < N; i++)
    for (j = 0; j <= i; j++)
      A[i*N+j] = (double)rand()/RAND_MAX;
    
  // Start timer
  printf("Start Krylov Subspace Construction\n");
  start = omp_get_wtime();
  // Start parallel section
#pragma omp parallel private(i,j,out,in) shared(A,w,h,v,tmp) firstprivate(M, N)
  {
  for (out = 0; out < M - 1; out++) {

    for (in = 0; in <= out; in++) {
#pragma omp single
      tmp = 0;
#pragma omp for reduction(+:tmp)
      for (i = 0; i < N; i++)
	for (j = 0; j < N; j++)
	  tmp += v[in][i]*A[i*N+j]*v[out][j];	  

#pragma omp single
      h[in] = tmp;
    }

#pragma omp single
    memset(w, 0, N*sizeof(double));
#pragma omp for
    for (i = 0; i < N; i++)
      for (j = 0; j < N; j++)
	w[i] += A[i*N+j]*v[out][j];
#pragma omp for
    for (i = 0; i < N; i++)
      for (in = 0; in <= out; in++)
	w[i] -= h[in]*v[in][i];
#pragma omp single
    tmp = 0;
#pragma omp for reduction(+:tmp)
    for (i = 0; i < N; i++)
      tmp += w[i]*w[i];
#pragma omp single
    h[out + 1] = tmp;

    if (h[out + 1] < 1e-7) {
      M = out + 1;
      printf("early convergence\n");
      break;
    }

#pragma omp for
    for (i = 0; i < N; i++)
      v[out+1][i] = w[i]/sqrt(h[out + 1]);    

  }
  }
  
  //End timer
  end   = omp_get_wtime();
  delta = ((double)(end-start));
  printf("Time to construct Krylov Subspace: %0.2fs\n", delta);

  // Test for subspace orthonormality
  for (out = 0; out < M; out++)
    for (in = 0; in < M; in++) {
      checksum = 0;
      for (i = 0; i < N; i++)
	checksum += v[out][i]*v[in][i];
      if (checksum > 1e-7)
	printf("%d %d check sum: %f\n", out, in, checksum);
    }

  return 0;
}
